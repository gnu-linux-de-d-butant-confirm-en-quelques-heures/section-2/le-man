# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser le manuel sur Linux

Ainsi nous verrons dans ce scénario, les commandes :

 - `man`: permettre d'afficher le manuel
 - `cat`: afficher le contenu d'un fichier
 - `grep`: permet d'afficher uniquement les lignes comportant un pattern spécifique

<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm --name man jassouline/man:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

# Exercice 1
 
1. En utilisant la commande `man`, essayez d'identifier l'option de la commande `cat` permettant d'afficher le numéro des lignes.

**Q1: Pour afficher le numéro des lignes on utilise l'option :**
- [ ] -n
- [ ] -E
- [ ] -s
- [ ] -T
<details><summary> Montrer la solution </summary>
<p>
L'option -n permet d'afficher le numéro des lignes. On peut également utiliser l'option --number.
</p>
</details>

2. Nous allons maintenant utiliser l'option de la question précédente pour connaître le numéro de ligne d'une phrase contenue dans un fichier.

**Q2: A quel numéro de ligne se situe la phrase: "syslog:x:104:108::/home/syslog:/bin/false" dans le fichier "/tmp/passwd" ?**

<details><summary> Montrer la solution </summary>
<p>
Il s'agit de la ligne numéro 23.
</p>
<p>
cat -n /tmp/passwd
</p>
</details>

<br/>

# Exercice 2
Dans ce deuxième exercice, nous allons essayer de comprendre comment fonctionne la commande `grep`.

Il s'agit d'une commande permettant de n'afficher à l'écran que la ligne contenant le mot recherché à l'intérieur d'un fichier. 

Commencez par faire une simple commande : `cat /tmp/passwd` pour afficher l'ensemble du contenu du fichier.

Si par exemple, on ne souhaite afficher QUE les lignes qui contiennent le mot **root**, on peut alors utiliser la commande : 
`grep root /tmp/passwd`

3. Effectuez la même chose pour n'afficher que les lignes contenant le mot **system** dans le fichier /tmp/passwd

<details><summary> Montrer la solution </summary>
<p>
grep system /tmp/passwd
</p>
</details>

4. Nous allons maintenant utiliser le man pour voir s'il existe une option permettant de compter le nombre de fois qu'un mot se situe dans un fichier.

**Q2: Pour le nombre de fois où un mot apparaît dans un fichier avec "grep" on utilise l'option :**
- [ ] -L
- [ ] -l
- [ ] -c
- [ ] -y

<details><summary> Montrer la solution </summary>
<p>
L'option -c permet d'afficher le nombre de fois qu'apparaît une occurence dans un fichier. On peut également utiliser l'option --count
</p>
</details>

5. Nous allons maintenant utiliser l'option de la question précédente pour connaître le nombre de fois qu'apparaît le mot **system** dans le fichier `/tmp/passwd`

**Q3: Combien de fois apparaît le mot "system" dans "/tmp/passwd" ?**

<details><summary> Montrer la solution </summary>
<p>
grep -c system /tmp/passwd

Le mot apparaît donc 4 fois dans le fichier.
</p>
</details>

6. On repète la commande précédente pour identifier le nombre de fois que la lettre **x** apparaît dans le fichier `/tmp/passwd`.

**Q4: Combien de fois apparaît la lettre "x" dans "/tmp/passwd" ?**

<details><summary> Montrer la solution </summary>
<p>
grep -c x /tmp/passwd

La lettre apparaît donc 31 fois dans le fichier /tmp/passwd.
</p>
</details>

# Conclusion

7. Tapez la commande `exit` pour sortir du conteneur, ou bien la combinaison de touches `CTRL + c`

