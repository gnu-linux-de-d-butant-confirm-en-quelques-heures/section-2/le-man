#Version 1.0

FROM ubuntu:22.10

ADD cat.sh /root/cat.sh

RUN chmod a+x /root/cat.sh && /root/cat.sh
RUN apt-get update && apt install man -y && yes | unminimize

CMD ["sleep", "infinity"]